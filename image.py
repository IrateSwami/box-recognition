import numpy
import cv2
import copy
import sys

# intake the image, copy it 
image_path = sys.argv[1]
original = cv2.imread(image_path)
image = original.copy()
another_original = original.copy()

# "denoise" the image
image = cv2.fastNlMeansDenoising(original, 10, 10, 7, 21)

# apply a gaussian blur, this is more noise reduction
image = cv2.GaussianBlur(image, (5,5), 0)

# find the median pixel value, this will help shape our image size for edge
# detection
avg = numpy.median(image)

# create the upper and lower boundaries of our image window
sigma = 0.33
lower = int(max(0, (1.0-sigma)*avg))
upper = int(min(255, (1.0-sigma)*avg))

# find the edges using the canny function
image = cv2.Canny(image, lower, upper)

# create a kernel to help with morph ops
kernel = numpy.ones((7,7), numpy.uint8)

# apply some morphological operations for noise redution and edge enhancement
image = cv2.morphologyEx(image, cv2.MORPH_CLOSE, kernel)

# flip the values in the image 
cv2.bitwise_not(image, image)

# now look for contours
contoured_img, contours, hierarchy = cv2.findContours(
        image, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) 

# sort contours list from largest to smallest
contours = sorted(contours, key=cv2.contourArea, reverse=True)

# draw the contours on the original image
cv2.drawContours(original, contours, -1, (0, 255, 0), 5)
#------------------------------------------------------------------------------
# this is basically a trick to merge all the contoured lines together
cv2.imwrite('temp.bmp', original)

# just read the image back in, we now have a summed up bunch of contours
image = cv2.imread('temp.bmp')

# filter out all colors that aren't (0,255,0)
green = numpy.array([0,255,0])
mask = cv2.inRange(image, green, green)

cv2.bitwise_not(mask, mask)

# now look for contours
contoured_img, contours, hierarchy = cv2.findContours(
        mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE) 

# sort contours list from largest to smallest
contours = sorted(contours, key=cv2.contourArea)

# draw the contours on the original image

contour_areas = 0

for c in contours:
    if cv2.contourArea(c) > 20000 and cv2.contourArea(c) < 40000:

        x, y, w, h = cv2.boundingRect(c)

        cv2.rectangle(another_original, (x, y), (x+w, y+w), (0, 255, 0), 2)

        print(cv2.contourArea(c))
        contour_areas += 1

print("contour areas:", contour_areas)

while True:
    cv2.imshow('box', another_original)

    if cv2.waitKey(1) & 0xFF == 27:
        break

cv2.destroyWindow('box')
